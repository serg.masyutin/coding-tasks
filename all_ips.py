def compute_all_ips(s: str) -> set[str]:
    def is_octet(s: str) -> bool:
        return int(s) <= 255

    def compute_N_octets(s: str, n: int, combinations: set[str]) -> bool:
        if n == 0:
            return len(s) == 0
        elif n == 1 and is_octet(s):
            combinations.add(s)
        elif len(s):
            start = 1
            while is_octet(s[:start]) and start < len(s):
                iteration_combinations = set()
                if compute_N_octets(s[start:], n - 1, iteration_combinations):
                    for combination in iteration_combinations:
                        combinations.add(f"{s[:start]}.{combination}")

                start += 1

        return True

    result = set()
    if compute_N_octets(s, 4, result):
        return result

    return set()


print(compute_all_ips("25525511135"))
