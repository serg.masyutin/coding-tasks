def longest_substring(s: str) -> str:
    def find_longest(s: str, found: set[str]) -> int:
        for end in range(0, len(s)):
            this_char = s[end]
            if this_char in found:
                return end

            found.add(this_char)

        return len(s)

    longest_start, longest_length = 0, 0
    for start in range(0, len(s)):
        length = 1 + find_longest(s[start + 1 :], {s[start]})
        if length > longest_length:
            longest_start = start
            longest_length = length

    return s[longest_start : longest_start + longest_length]


print(longest_substring(""))
print(longest_substring("abcabcdebb"))
print(longest_substring("aabbabc"))
